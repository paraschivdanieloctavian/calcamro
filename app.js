const express = require('express');
const request = require('request');
const bodyParser = require('body-parser');
const path = require('path');

const app = express();

//body parser
app.use(bodyParser.urlencoded({ extended: true }));
//static folder
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res) {
	res.render('index.html');
});
// sign up route
app.post('/signup', function(req, res) {
	const { name, email, phone, experienta, salariu } = req.body;
	console.log(req.body);
	//validation empty
	if (!name || !email || !phone) {
		res.redirect('/fail.html');
		return;
	}

	//Construct req data
	const data = {
		members: [
			{
				email_address: email,
				status: 'subscribed',
				merge_fields: {
					FNAME: name,
					PHONE: phone,
					EXP: experienta,
					SAL: salariu
				}
			}
		]
	};

	const postData = JSON.stringify(data);

	const options = {
		url: 'https://us3.api.mailchimp.com/3.0/lists/6a1b617fb1',
		method: 'POST',
		headers: {
			Authorization: 'auth bed6532993a3f2686482b99a4566bcc5-us3'
		},
		body: postData
	};

	request(options, (err, response, body) => {
		if (err) {
			res.redirect('/fail.html');
		} else {
			if (response.statusCode === 200) {
				res.redirect('/success.html');
			} else {
				res.redirect('/fail.html');
			}
		}
	});
});

// const PORT = process.env.PORT || 5000;

// app.listen(PORT, console.log('Server started on ${PORT}'));

const http = require('http');
const hostname = '95.217.57.150';
const port = 80;

const server = http.createServer((req, res) => {
	res.statusCode = 200;
	res.setHeader('Content-Type', 'text/plain');
	res.end('Hello World! NodeJS \n');
});

server.listen(port, hostname, () => {
	console.log(`Server running at http://${hostname}:${port}/`);
});
